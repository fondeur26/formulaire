<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Document</title>
        <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;600&display=swap" rel="stylesheet">
        <style>
            .bouton {
                 height: 100%;
                width: 100%;
                padding-left: 15px;
                font-size: 18px;
                 outline: none;
                background: none;
                color: #070707;
                border: 2px solid #99f309;}
            
        </style>
    </head>
    <body>

<!-- Comment valider les données en provenance de mon formulaire ? -->
<div class="formul">

<h1 style="color:#000000">Qui êtes vous ?</h1>



<form action="formulaire.php" method="post">
    
    <p><i>Complétez le formulaire. Les champs marqué par </i><em>*</em> sont <em>obligatoires</em></p>
    <fieldset style="color:#000000">
         <legend>Contact</legend><br>
        
        <label>Prénom <em>*</em></label><br><br>
        <input type="text" name="prenom" pattern="[A-Za-z-]{2,}" maxlength="50" required><br><br>
        
        <label>Nom <em>*</em></label><br><br>
        <input type="text" name="nom" pattern="[A-Za-z-]{2,}" maxlength="50" required><br><br>
        
        <label>Portable</label>
        <br><br>
        <input id="telephone" name="telephone" type="tel" placeholder="06xxxxxxxx" pattern="[0]{1}[1-7]{1}[0-9]{8}"><br><br>
        
        <label>Email <em>*</em></label><br><br>
        <input id="email" name="email" type="email" placeholder="Email" required pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$"><br>
        <br>
        
        <label for="message">Laissez nous un message : <em>*</em></label><br><br>
        <textarea id="message" name="message" rows="5" cols="33" required pattern="[A-Za-z]{2,}" maxlenght="1000"></textarea>
    </fieldset>

    <p><input class="bouton" type="submit" value="Envoyer"></p>
</form>

</div>
</body>
</html>
<?php
//1 Les données obligatoires sont elles pr�sentes ?
if (!empty($_POST)) { 

if( isset( $_POST["prenom"]) && isset( $_POST["nom"]) && isset( $_POST["email"]) && isset( $_POST["message"]) && isset( $_POST["telephone"])){
 
    //2 Les données obligatoires sont-elles remplies ?
    if( !empty( $_POST["prenom"]) && !empty( $_POST["nom"]) && !empty( $_POST["email"]) && !empty( $_POST["message"])){
 
        //3 Les données obligatoires ont-elles une valeur conforme  la forme attendue ?
        if( filter_var($_POST["email"], FILTER_SANITIZE_EMAIL)){
 
            $_POST["email"] = filter_var($_POST["email"], FILTER_SANITIZE_EMAIL);
 
            if( filter_var($_POST["message"], FILTER_SANITIZE_FULL_SPECIAL_CHARS)){
                $_POST["message"] = filter_var($_POST["message"], FILTER_SANITIZE_FULL_SPECIAL_CHARS);
 
                if( filter_var($_POST["prenom"], FILTER_SANITIZE_STRING) && filter_var($_POST["nom"], FILTER_SANITIZE_STRING) && filter_var($_POST["telephone"], FILTER_SANITIZE_STRING)) {
                    $_POST["prenom"] = filter_var($_POST["prenom"], FILTER_SANITIZE_STRING);
                    $_POST["nom"] = filter_var($_POST["nom"], FILTER_SANITIZE_STRING);
                    $_POST["telephone"] = filter_var($_POST["telephone"], FILTER_SANITIZE_STRING);
                  
 
                    //4 Les donn�es optionnelles sont-elles pr�sentes ? Les donn�es optionnelles sont-elles remplies ? Les donn�es optionnelles ont-elles une valeur conforme � la forme attendue ?                
                 
 
                    // if( isset($_POST["telephone"])){
                    //     $_POST["telephone"] = filter_var($_POST["telephone"], FILTER_SANITIZE_STRING);
                    // }else{print("Votre message contient sans doute des caractéres non appropriés");
                    // }
 
                    //5 afficher le mail et le message
                    printf('Bonjour %s, votre message a &eacute;t&eacute; envoy&eacute; et vous serez contact&eacute; dans les plus bref d&eacute;lais ! ', $_POST["prenom"]);
 
                    //6 utiliser la fonction mail() pour envoyer le message vers botre bo�te mail
                    $to = "fondeur.rv@free.fr";
                    $subject = "Vous avez un nouveau contact";
                    $message = $_POST["prenom"] . ' ' . $_POST["nom"].' vous a écrit<br>';
                    $message .= PHP_EOL . '<br><br>Contenu du message : <br>' . $_POST["message"];
                    if( $_POST["telephone"]){
                        $message .= PHP_EOL . '<br>Téléphone : ' . $_POST["telephone"]. '<br>Email : '. $_POST["email"];}
                    $headers = 'From: Herve.FONDEUR@labo-ve.fr' . PHP_EOL .
                    'Reply-To: ' .$_POST["email"]. PHP_EOL .
                    'Content-Type: text/html; charset=UTF-8'. PHP_EOL .
                    'X-Mailer: PHP/' . phpversion() ;
                    mail($to, $subject, $message, $headers);
                    
                    
 
                }else {print("Il semble que les données que vous avez saisies ne soient pas valides");
                }
 
            }else {print("Votre message contient sans doute des caractéres non appropriés");
            }
 
        }else {print("Merci de saisir une adresse email valide");
        }
 
    }else {print("Merci de remplir les champs obligatoires");
    }
 
}else {print("ok");
}
 
}